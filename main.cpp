#include <iostream>
#include <string>
#include <vector>


using namespace std;



template<typename R>
class result {
private:
    R value;
    string err_msg;
    result(R value, string err) : value(value), err_msg(err) {
        
    }
public:
    
    static result error(string st) {
        return result(0, st);
    }
     static result ok(R val) {
        return result(val, "");
    }

    //is ok
    bool is_ok() {
        return err_msg != "";
    }
    //get value
    R get_value() {
        return value;
    }
    //get error message
    string get_error() {
        return string(err_msg);
    }
};

//class stack
template<typename T>
class m_stack {
    T* stack;
    int size;
    int top;
public:
    //stack constructor
    m_stack(int s) {
        size = s;
        stack = new T[size];
        top = -1;
    }
    void push(T data) {
        stack[++top] = data;
    }
    result<T> pop() {
        if (top == -1) {
            return result<T>::error("Stack is empty");
        }
        return result<T>::ok(stack[top--]);
    }
};


int main() {
    vector<int> ar = {15, 12, 7, 10, 4, 2, 1};
   
    m_stack<int> st(ar.size());
    for (int i = 0; i < ar.size(); i++) {
        st.push(ar[i]);
    }

    for (int i = 0; i < ar.size(); i++) {
        auto x = st.pop();
        cout << x.get_value() << " " << x.get_error() << endl;
    }
    cout << endl;
    result<int> r = st.pop();
    cout << "Error: " <<r.get_error() << endl;
    return 0;
}